#/bin/bash
RED='\033[0;31m'
GRN='\033[0;32m'
NC='\033[0m' # No Color

printf "${GRN}This is the first file you should run, that installs Arch Linux on your system.${NC}\n"

lsblk

printf "Which HDD do you want to install Arch Linux to?\n"
read disk
export disk

printf "${RED}Make sure to delete all filesystem signatures manually, otherwise the installation will fail!${NC}\n"


echo "Checking internet connection... if it fails, use an ethernet cord!"
#ping -c 5 google.com #TODO: re-enable when done with debugging

timedatectl set-ntp true

printf "${RED}Continuing will erase your hard drive!\nAre you sure you want to continue?${NC}[y/n]\n"
read cont

if [ $cont = 'n' ]
then
    echo "Exiting!"
    exit

elif [ $cont = 'y' ]
then
    echo "Continuing!"
    #TODO: make this follow plan.txt's disk sizes
    ( # make /boot or /dev/sdX1 # needs to be ext4
        echo o
        echo n
        echo p
        echo 1
        echo
        echo +2G
        echo y
        echo w
    ) | sudo fdisk /dev/$disk

    ( # make root ( / ) or /dev/sdX2 # needs to be ext4
        echo n
        echo p
        echo 2
        echo 
        echo +25G
        echo y
        echo w
    ) | sudo fdisk /dev/$disk
    ramsize=`free | awk '{print $2}' | sed -n 2p`
    swapsize=`echo $ramsize | awk '{print $1 / 1024 / 1024}' | awk '{print $1 * 1.5}'`
    ( # make swap partition or /dev/sdX3 
        echo n
        echo p
        echo 3
        echo
        echo +`printf "%.0f" "$swapsize"`G
        echo y
        echo w
    ) | sudo fdisk /dev/$disk 
    ( # make /home or /dev/sdX4 # needs to be ext4
        echo n
        echo p
        echo
        echo
        echo y
        echo w
    ) | sudo fdisk /dev/$disk

    mkfs.ext4 /dev/"$disk"1
    mkfs.ext4 /dev/"$disk"2
    mkfs.ext4 /dev/"$disk"4
    mkswap /dev/"$disk"3
    swapon /dev/"$disk"3
    mount /dev/"$disk"2 /mnt
    mkdir /mnt/boot
    mkdir /mnt/home
    mount /dev/"$disk"4 /mnt/home
    mount /dev/"$disk"1 /mnt/boot

    pacstrap /mnt base base-devel wget grub networkmanager

    genfstab -U /mnt >> /mnt/etc/fstab
    
    echo "Make sure to download the second file!"
    
    arch-chroot /mnt 
else
    echo "you did not type a valid letter!"
    exit #TODO: allow user to retype
fi